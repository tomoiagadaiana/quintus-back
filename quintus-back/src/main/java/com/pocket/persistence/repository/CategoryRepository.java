package com.pocket.persistence.repository;

import com.pocket.persistence.entity.Category;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CategoryRepository extends CrudRepository<Category, Integer> {
Category findByName(String name);
}

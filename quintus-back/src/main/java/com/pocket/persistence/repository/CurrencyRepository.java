package com.pocket.persistence.repository;

import com.pocket.persistence.entity.Account;
import com.pocket.persistence.entity.Currency;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface CurrencyRepository extends CrudRepository<Currency, Integer> {
    Optional<Currency> findById(int id);
    Currency findCurrencyByName(String name);

}

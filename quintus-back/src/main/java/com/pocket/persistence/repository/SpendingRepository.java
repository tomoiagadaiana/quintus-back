package com.pocket.persistence.repository;

import com.pocket.persistence.entity.Account;
import com.pocket.persistence.entity.Currency;
import com.pocket.persistence.entity.Spending;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface SpendingRepository extends CrudRepository<Spending, Integer> {

    List<Spending> findAll();

}

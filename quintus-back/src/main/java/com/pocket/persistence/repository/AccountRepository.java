package com.pocket.persistence.repository;
import com.pocket.persistence.entity.Account;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface AccountRepository extends CrudRepository<Account, Integer> {

Optional<Account> findById(int id);
List<Account> findByUserId(int id);
}

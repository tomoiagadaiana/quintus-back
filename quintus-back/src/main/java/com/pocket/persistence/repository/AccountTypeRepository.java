package com.pocket.persistence.repository;

import com.pocket.persistence.entity.AccountType;
import com.pocket.persistence.entity.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface AccountTypeRepository extends CrudRepository<AccountType,Integer> {
    List<AccountType> findAll();
}

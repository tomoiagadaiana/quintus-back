package com.pocket.persistence.entity;

import javax.persistence.*;

@Entity
@Table(name = "account_type")
public class AccountType {

    @Id
    @GeneratedValue
    private int id;


    @Column(name = "title")
    private String title;



    public AccountType(){

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public AccountType(String title) {
        this.title = title;
    }
}


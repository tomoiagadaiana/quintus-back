package com.pocket.business.exception;

public enum ExceptionCode {

    ACCOUNT_NOT_EXIST(1014,"The account does not exist."),
    UNKNOWN_EXCEPTION(1000, "Unknown"),
    USER_VALIDATION_EXCEPTION(1001, "Validation Exception"),
    ENCRYPT_FAIL(1002, "Password encryption failed."),
    PASSWORD_NOT_VALID(1003, "Password not valid."),
    EMAIL_NOT_VALID(1004, "Email not valid."),
    USER_NOT_EXIST(1011, "The user does not exist"),
    EMAIL_EXIST_ALREADY(1001, "Email already exists."),
    ACCOUNTS_NOT_EXIST(1013, "Accounts not exist."),
    ACCOUNT_TYPE_NULL(1005, "Account type value is null."),
    BUDGET_NOT_VALID(1009, "Budget value should be greater than 0."),
    ACCOUNT_IS_NULL(1010, "Account not valid"),
    CATEGORY_IS_NULL(1011, "Category not valid"),
    CATEGORY_NAME(1090, "Can not add a category with the same name."),
    CURRENCY_IS_NULL(1012, "Currency not valid"),
    CURRENCY_NAME(1017, "Can not add a currency with the same name");
    int id;
    String message;

    ExceptionCode(int id, String message) {
        this.id = id;
        this.message = message;
    }

    public int getId() {
        return id;
    }

    public String getMessage() {
        return message;
    }

}

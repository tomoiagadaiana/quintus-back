package com.pocket.business.service;

import com.pocket.business.dto.UserDTO;
import com.pocket.business.dto.UserDTOTransform;
import com.pocket.business.exception.BusinessException;
import com.pocket.business.exception.ExceptionCode;
import com.pocket.business.utils.Encryptor;
import com.pocket.persistence.entity.User;
import com.pocket.persistence.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;
    private UserDTOTransform userDTOTransform;


    public UserDTO register(UserDTO userDTO) throws BusinessException {
        validForRegister(userDTO);
        User user = userDTOTransform.toEntity(userDTO);
        user.setPassword(Encryptor.encrypt(userDTO.getPassword()));
        userRepository.save(user);
        return userDTOTransform.fromEntity(user);
    }

    public UserDTO addEmployee(UserDTO userDTO) throws BusinessException {
        validForRegister(userDTO);
        User user = userDTOTransform.toEntity(userDTO);
        user.setPassword(Encryptor.encrypt(userDTO.getPassword()));
        userRepository.save(user);
        return userDTOTransform.fromEntity(user);
    }

    public UserDTO editUser(UserDTO user) throws BusinessException {
        Optional<User> userFind = userRepository.findById(user.getId());
        if (userFind.isPresent()) {
            User oldUser = userFind.get();
            oldUser.setEmail(user.getEmail());
            oldUser.setPassword(user.getPassword());
            User userSaved = userRepository.save(oldUser);
            return userDTOTransform.fromEntity(userSaved);
        } else {
            throw new BusinessException(ExceptionCode.USER_NOT_EXIST);
        }
    }

    public UserDTO deactivateUser(String email) throws BusinessException {
        Optional<User> user = userRepository.findByEmail(email);
        if (user.isPresent()) {
            User user1 = user.get();


            User userUpdate = userRepository.save(user1);
            UserDTO userDTO = userDTOTransform.fromEntity(userUpdate);
            return userDTO;
        } else {
            throw new BusinessException(ExceptionCode.EMAIL_NOT_VALID);
        }
    }


    public UserDTO login(String email, String password) throws BusinessException {
        Optional<User> usersByEmail = userRepository.findByEmail(email);
        if (!usersByEmail.isPresent()) {
            throw new BusinessException(ExceptionCode.EMAIL_NOT_VALID);

        }
//        if (!Encryptor.encrypt(password).equals(usersByEmail.get().getPassword())) {
////            throw new BusinessException(ExceptionCode.PASSWORD_NOT_VALID);
////        }
        if (!password.equals(usersByEmail.get().getPassword())) {
            throw new BusinessException(ExceptionCode.PASSWORD_NOT_VALID);
        }

        User user = usersByEmail.get();
        return userDTOTransform.fromEntity(user);
    }

    public List<UserDTO> getAllUser() {
        List<UserDTO> users = new ArrayList<UserDTO>();
        userRepository.findAll().forEach(user -> {
            users.add(userDTOTransform.fromEntity(user));
        });
        return users;
    }

    private boolean isValidEmail(String email) {
        final Pattern validEmailAddressRegex =
                Pattern.compile("^[A-Z0-9._%+-]+@gmail.com$", Pattern.CASE_INSENSITIVE);

        Matcher matcher = validEmailAddressRegex.matcher(email);
        return matcher.find();
    }


//    private boolean isValidPhoneNumber(String number) {
//        final Pattern validEmailAddressRegex =
//                Pattern.compile("^(\\+4|)?(07[0-8]{1}[0-9]{1}|02[0-9]{2}|03[0-9]{2}){1}?(\\s|\\.|\\-)?([0-9]{3}(\\s|\\.|\\-|)){2}$",
//                        Pattern.CASE_INSENSITIVE);
//
//        Matcher matcher = validEmailAddressRegex.matcher(number);
//        return matcher.find();
//    }

    private boolean validateFields(UserDTO user) {

        return  user.getPassword() != null
                && isValidEmail(user.getEmail());
    }

    private void validForRegister(UserDTO user) throws BusinessException {
        if (userRepository.findByEmail(user.getEmail()).isPresent()) {
            throw new BusinessException(ExceptionCode.EMAIL_EXIST_ALREADY);
        }
        if (!validateFields(user)) {
            throw new BusinessException(ExceptionCode.USER_VALIDATION_EXCEPTION);
        }
    }


}

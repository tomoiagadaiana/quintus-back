package com.pocket.business.service;

import com.pocket.business.dto.AccountDTO;
import com.pocket.business.dto.AccountDTOTransform;
import com.pocket.business.exception.BusinessException;
import com.pocket.business.exception.ExceptionCode;
import com.pocket.persistence.entity.Account;
import com.pocket.persistence.entity.User;
import com.pocket.persistence.repository.AccountRepository;
import com.pocket.persistence.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class AccountService {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private UserRepository userRepository;
    private AccountDTOTransform accountDTOTransform;


    public List<AccountDTO> getAllAccount(String email) throws BusinessException {
        Optional<User> user = userRepository.findByEmail(email);
        List<AccountDTO> accountDTOS = new ArrayList<AccountDTO>();
        List<Account> accounts = accountRepository.findByUserId(user.get().getId());
        accounts.forEach(account -> {
            accountDTOS.add(accountDTOTransform.fromEntity(account));
        });
        if (accountDTOS.isEmpty()) {
            throw new BusinessException(ExceptionCode.ACCOUNTS_NOT_EXIST);
        }
        return accountDTOS;
    }

    public AccountDTO updateAccount(AccountDTO accountDTO) throws BusinessException {
        Optional<Account> accountFind = accountRepository.findById(accountDTO.getId());
        if (accountFind.isPresent()) {
            Account account = accountFind.get();
            if (accountDTO.getAccountType() != null) {
                account.setType(accountDTO.getAccountType());
            } else {
                throw new BusinessException(ExceptionCode.ACCOUNT_TYPE_NULL);
            }
            if (accountDTO.getBudget()>0) {
                account.setBudget(accountDTO.getBudget());
            } else {
                throw new BusinessException(ExceptionCode.BUDGET_NOT_VALID);
            }
            Account accountSaved = accountRepository.save(account);
            return accountDTOTransform.fromEntity(accountSaved);
        } else {
            throw new BusinessException(ExceptionCode.ACCOUNT_NOT_EXIST);
        }

    }

    public AccountDTO insertAccount(AccountDTO accountDTO) throws BusinessException{

        if(accountDTO == null){
            throw new BusinessException(ExceptionCode.ACCOUNT_IS_NULL);
        } else {
            Account account = accountDTOTransform.toEntity(accountDTO);
            accountRepository.save(account);
            return accountDTOTransform.fromEntity(account);
        }
    }

    public AccountDTO delete(int id) throws BusinessException {
        if((Integer) id == null) {
            throw new BusinessException(ExceptionCode.ACCOUNT_NOT_EXIST);
        }

        Optional<Account> account = accountRepository.findById(id);
        if(!account.isPresent()){
            throw new BusinessException(ExceptionCode.ACCOUNT_NOT_EXIST);
        }else{
            accountRepository.delete(account.get());
            AccountDTO accountDTO = accountDTOTransform.fromEntity(account.get());
            return  accountDTO;
        }
    }





}

package com.pocket.business.service;

import com.pocket.business.dto.AccountDTO;
import com.pocket.business.dto.CurrencyDTO;
import com.pocket.business.dto.CurrencyDTOTransform;
import com.pocket.business.exception.BusinessException;
import com.pocket.business.exception.ExceptionCode;
import com.pocket.persistence.entity.Account;
import com.pocket.persistence.entity.Currency;
import com.pocket.persistence.repository.CurrencyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CurrencyService {
    @Autowired
    private CurrencyRepository currencyRepository;

    private CurrencyDTOTransform currencyDTOTransform;

    public CurrencyDTO insertCurrency (CurrencyDTO currencyDTO) throws BusinessException {

        if(currencyDTO == null){
            throw new BusinessException(ExceptionCode.CURRENCY_IS_NULL);
        } else {
            Currency currency = currencyDTOTransform.toEntity(currencyDTO);
            if (verifyExist(currency.getName()) == false) {
                currencyRepository.save(currency);
                return currencyDTOTransform.fromEntity(currency);
            } else {
                throw new BusinessException(ExceptionCode.CURRENCY_NAME);
            }
        }
    }

    public boolean verifyExist( String name){
        Currency currency = currencyRepository.findCurrencyByName(name);
        if(currency == null){
            return false;
        } else {
            return true;
        }
    }


}

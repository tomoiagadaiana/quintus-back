package com.pocket.business.service;

import com.pocket.business.dto.CategoryDTO;
import com.pocket.business.dto.CategoryDTOTransform;
import com.pocket.business.exception.BusinessException;
import com.pocket.business.exception.ExceptionCode;
import com.pocket.persistence.entity.Category;
import com.pocket.persistence.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;
    private CategoryDTOTransform categoryDTOTransform;


    public CategoryDTO insertCategory(CategoryDTO categoryDTO) throws BusinessException {

        if(categoryDTO != null ){
            throw new BusinessException(ExceptionCode.CATEGORY_IS_NULL);
        } else {
            Category category = categoryDTOTransform.toEntity(categoryDTO);
            if(thereIsCategory(category.getName()) == true){
                throw new BusinessException(ExceptionCode.CATEGORY_NAME);
            } else {
                categoryRepository.save(category);
                return categoryDTOTransform.fromEntity(category);
            }

        }
    }

    public boolean thereIsCategory(String name){
        Category category = categoryRepository.findByName(name);
        if(category == null){
            return true;
        } else {
            return false;
        }
    }






}

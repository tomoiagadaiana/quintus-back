package com.pocket.business.service;

import com.pocket.business.dto.AccountTypeDTO;
import com.pocket.business.dto.AccountTypeTransform;
import com.pocket.persistence.repository.AccountTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AccountTypeService {

    @Autowired
    private AccountTypeRepository accountTypeRepository;
    private AccountTypeTransform accountTypeTransform;

    public List<AccountTypeDTO> getAllAccountType() {
        List<AccountTypeDTO> accountsType = new ArrayList<AccountTypeDTO>();
        accountTypeRepository.findAll().forEach(accountType -> {
            accountsType.add(accountTypeTransform.fromEntity(accountType));
        });
        return accountsType;
    }
}

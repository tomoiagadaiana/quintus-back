package com.pocket.business.service;

import com.pocket.business.dto.SpendingDTO;
import com.pocket.business.dto.SpendingDTOTransform;
import com.pocket.persistence.repository.SpendingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SpendingService {

    @Autowired
    private SpendingRepository spendingRepository;
    private SpendingDTOTransform spendingDTOTransform;


    public List<SpendingDTO> getAllSpendings(){
        List<SpendingDTO> spendingDTOS = new ArrayList<SpendingDTO>();
        spendingRepository.findAll().forEach(spending -> {
            spendingDTOS.add(spendingDTOTransform.fromEntity(spending));
        });
        return spendingDTOS;
    }
}

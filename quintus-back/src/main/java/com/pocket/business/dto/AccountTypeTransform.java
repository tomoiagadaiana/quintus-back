package com.pocket.business.dto;

import com.pocket.persistence.entity.AccountType;

public class AccountTypeTransform {

    public AccountTypeTransform() {
    }

    public static AccountTypeDTO fromEntity(AccountType accountType){
        AccountTypeDTO accountTypeDTO = new AccountTypeDTO();
        accountTypeDTO.setId(accountType.getId());
        accountTypeDTO.setTitle(accountType.getTitle());

        return accountTypeDTO;
    }

    public static AccountType toEntity(AccountTypeDTO accountTypeDTO){
        AccountType accountType = new AccountType();
        accountType.setTitle(accountTypeDTO.getTitle());
        return accountType;

    }
}

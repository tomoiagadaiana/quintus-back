package com.pocket.business.dto;

import com.pocket.persistence.entity.Spending;

public class SpendingDTOTransform {
    public SpendingDTOTransform(){

    }

    public static  Spending toEntity(SpendingDTO spendingDTO){
        Spending spending = new Spending();
        spending.setAccount(spendingDTO.getAccount());
        spending.setAmount(spendingDTO.getAmount());
        spending.setDate(spendingDTO.getDate());
        spending.setString(spendingDTO.getString());
        spending.setCategory(spendingDTO.getCategory());
        return spending;
    }
    public static SpendingDTO fromEntity(Spending spending){
        SpendingDTO spendingDTO = new SpendingDTO();
        spendingDTO.setAccount(spending.getAccount());
        spendingDTO.setAmount(spending.getAmount());
        spendingDTO.setCategory(spendingDTO.getCategory());
        spendingDTO.setDate(spending.getDate());
        spendingDTO.setString(spending.getString());
        return spendingDTO;
    }
}

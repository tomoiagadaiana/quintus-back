package com.pocket.business.dto;

import com.pocket.persistence.entity.Account;
import com.pocket.persistence.entity.Category;

import java.sql.Date;

public class SpendingDTO {

    private int id;
    private Date date;
    private Account account;
    private double amount;
    private Category category;
    private String string;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getString() {
        return string;
    }

    public void setString(String string) {
        this.string = string;
    }
}

package com.pocket.business.dto;

import com.pocket.persistence.entity.AccountType;
import com.pocket.persistence.entity.User;

public class AccountDTO {

    private int id;
    private double budget;
    private AccountType accountType;
    private User user;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getBudget() {
        return budget;
    }

    public void setBudget(double budget) {
        this.budget = budget;
    }

    public AccountType getAccountType() {
        return accountType;
    }

    public void setAccountType(AccountType accountType) {
        this.accountType = accountType;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}

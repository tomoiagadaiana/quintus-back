package com.pocket.business.dto;

import com.pocket.persistence.entity.AccountType;
import com.pocket.persistence.entity.Currency;

public class CurrencyDTOTransform {
    public CurrencyDTOTransform() {
    }

    public static CurrencyDTO fromEntity(Currency currency){
        CurrencyDTO currencyDTO = new CurrencyDTO();
        currencyDTO.setId(currency.getId());
        currencyDTO.setName(currency.getName());

        return currencyDTO;
    }

    public static Currency toEntity(CurrencyDTO currencyDTO){
        Currency currency = new Currency();
        currency.setName(currencyDTO.getName());
        return currency;

    }
}


package com.pocket.business.dto;

import com.pocket.persistence.entity.Category;

public class CategoryDTOTransform {

    public CategoryDTOTransform(){

    }

    public static CategoryDTO fromEntity(Category category){
        CategoryDTO categoryDTO = new CategoryDTO();
        categoryDTO.setId(category.getId());
        categoryDTO.setName(category.getName());
        return categoryDTO;
    }

    public static Category toEntity(CategoryDTO categoryDTO){
        Category category = new Category();
        category.setId(categoryDTO.getId());
        category.setName(categoryDTO.getName());
        return category;
    }

}

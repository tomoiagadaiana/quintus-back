package com.pocket.business.dto;

import com.pocket.persistence.entity.Account;

public class AccountDTOTransform {

    private AccountDTOTransform() {

    }

    public static AccountDTO fromEntity(Account account) {
        AccountDTO accountDTO = new AccountDTO();
        accountDTO.setId(account.getId());
        accountDTO.setAccountType(account.getType());
        accountDTO.setBudget(account.getBudget());
        accountDTO.setUser(account.getUser());
        return accountDTO;
    }

    public static Account toEntity(AccountDTO accountDTO) {
        Account account = new Account();
        account.setBudget(accountDTO.getBudget());
        account.setType(accountDTO.getAccountType());
        account.setUser(accountDTO.getUser());
        return account;
    }

}

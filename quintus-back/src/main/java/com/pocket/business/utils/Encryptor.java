package com.pocket.business.utils;

import com.pocket.business.exception.BusinessException;
import com.pocket.business.exception.ExceptionCode;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.security.Key;

public class Encryptor {
    private static final String KEY = "Bar12345Bar12345";

    private Encryptor() {

    }

    public static String encrypt(String toEncrypt) throws BusinessException {
        String encryptedString = null;

        try {
            Key aesKey = new SecretKeySpec(KEY.getBytes(), "AES");
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.ENCRYPT_MODE, aesKey);
            byte[] encrypted = cipher.doFinal(toEncrypt.getBytes());
            encryptedString = new String(encrypted);
        } catch (Exception e) {
            throw new BusinessException(ExceptionCode.ENCRYPT_FAIL);
        }
        return encryptedString;
    }
}

package com.pocket.business.controller;


import com.pocket.business.dto.AccountDTO;
import com.pocket.business.exception.BusinessException;
import com.pocket.business.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/account")
public class AccountController {

    @Autowired
    AccountService accountService;

    @GetMapping("/allAccounts")
    public List<AccountDTO> getAllAccounts(@RequestParam("email") String email) throws BusinessException {
        System.out.println("daa get");
        return accountService.getAllAccount(email);
    }

    @PutMapping("/updateAccount")
    public ResponseEntity<?> updateAccount(@RequestBody AccountDTO accountDTO) {
        System.out.println("da, update");
        try {
            System.out.println(accountDTO.getBudget());
            AccountDTO accountDTO1 = accountService.updateAccount(accountDTO);
            return new ResponseEntity<AccountDTO>(accountDTO1, HttpStatus.OK);
        } catch (BusinessException e) {
            return new ResponseEntity<>(e.getExceptionCode(), HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(value = "insert")
    public ResponseEntity<?> insert(@RequestBody AccountDTO accountDTO){
        try{
            AccountDTO accountDTO1 = accountService.insertAccount(accountDTO);
            return  new ResponseEntity<>(accountDTO1, HttpStatus.OK);
        } catch (BusinessException b){
            return new ResponseEntity<>(b.getExceptionCode(),HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping(value = "/delete")
    public ResponseEntity<?> deleteAccount(@RequestParam("id") int id){
        try{
            AccountDTO accountDTO = accountService.delete(id);
            return new ResponseEntity<>(accountDTO,HttpStatus.OK);
        }catch (BusinessException b){
            return new ResponseEntity<>(b, HttpStatus.BAD_REQUEST);
        }
    }
}

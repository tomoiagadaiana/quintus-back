package com.pocket.business.controller;
import com.pocket.business.dto.CategoryDTO;
import com.pocket.business.exception.BusinessException;
import com.pocket.business.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/category")
public class CategoryController {
    @Autowired
    CategoryService categoryService;

    @PostMapping(value = "insert")
    public ResponseEntity<?> insert(@RequestBody CategoryDTO categoryDTO){
        try{
            CategoryDTO categoryDTO1 = categoryService.insertCategory(categoryDTO);
            return  new ResponseEntity<>(categoryDTO1, HttpStatus.OK);
        } catch (BusinessException b){
            return new ResponseEntity<>(b.getExceptionCode(),HttpStatus.BAD_REQUEST);
        }
    }
}

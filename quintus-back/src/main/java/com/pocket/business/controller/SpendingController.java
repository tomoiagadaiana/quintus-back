package com.pocket.business.controller;

import com.pocket.business.dto.SpendingDTO;
import com.pocket.business.service.SpendingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/spending")
public class SpendingController {

    @Autowired
    SpendingService spendingService;


    @GetMapping("/getAll")
    public List<SpendingDTO> getAllSpending(){
        return  spendingService.getAllSpendings();

    }
}



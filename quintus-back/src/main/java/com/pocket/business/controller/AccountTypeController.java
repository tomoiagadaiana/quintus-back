package com.pocket.business.controller;

import com.pocket.business.dto.AccountTypeDTO;
import com.pocket.business.service.AccountTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/account")
public class AccountTypeController {

    @Autowired
    private AccountTypeService accountTypeService;

    @GetMapping("/allAccountsType")
    public List<AccountTypeDTO> getAllAccountsType() {
        long time = System.currentTimeMillis();
        return accountTypeService.getAllAccountType();
    }
}

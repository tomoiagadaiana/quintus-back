package com.pocket.business.controller;

import com.pocket.business.dto.UserDTO;
import com.pocket.business.exception.BusinessException;
import com.pocket.business.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;


import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/home")
public class UserController {

    @Autowired
     UserService userService;

    @GetMapping("/users")
    public List<UserDTO>  getAllUsers(){
        return  userService.getAllUser();

    }



    @PutMapping("/editUser")
    public ResponseEntity<?> editUser(@RequestBody UserDTO userDTO) {
        long time = System.currentTimeMillis();
        try {
            UserDTO userDTO1 = userService.editUser(userDTO);
            return new ResponseEntity<UserDTO>(userDTO1,HttpStatus.OK);
        } catch (BusinessException e) {
            return new ResponseEntity<>(e.getExceptionCode(),HttpStatus.BAD_REQUEST);
        }
    }



    @PostMapping(value = "/users/register")
    public ResponseEntity<?> register(@RequestBody UserDTO userDTO){
        long time = System.currentTimeMillis();
        try{
            UserDTO userDTO1 = userService.register(userDTO);

            return new ResponseEntity<>(userDTO1,HttpStatus.OK);
        }catch(BusinessException b){

            return new ResponseEntity<>(b.getExceptionCode(),HttpStatus.BAD_REQUEST);
        }finally {
            time = (System.currentTimeMillis() - time);
            System.out.println("time users/register:" +time+"ms");
        }
    }

    @PostMapping(value = "/addEmployee")
    public ResponseEntity<?> addEmployee(@RequestBody UserDTO userDTO){
        long time = System.currentTimeMillis();
        try{
            UserDTO userDTO1 = userService.addEmployee(userDTO);

            return new ResponseEntity<>(userDTO1,HttpStatus.OK);
        }catch(BusinessException b){

            return new ResponseEntity<>(b.getExceptionCode(),HttpStatus.BAD_REQUEST);
        }finally {
            time = (System.currentTimeMillis() - time);
            System.out.println("time users/register:" +time+"ms");
        }
    }

    @PostMapping(value = "/login")
    public ResponseEntity<?> login (@RequestParam("email") String email, @RequestParam("password") String password){
        long time = System.currentTimeMillis();
        try{
            UserDTO userDTO = userService.login(email,password);
            return new ResponseEntity<>(userDTO,HttpStatus.OK);
        }catch(BusinessException b){
            return new ResponseEntity<>(b.getExceptionCode(),HttpStatus.BAD_REQUEST);
        }finally {
            time = (System.currentTimeMillis() - time);
            System.out.println("time login:" +time+"ms");
        }
    }

    @PostMapping(value = "/deactivateUser")
    public ResponseEntity<?> deactivateUser(@RequestParam("email") String email){
        long time = System.currentTimeMillis();
        try{
            UserDTO user =userService.deactivateUser(email);
            return new ResponseEntity<>(user,HttpStatus.OK);
        }catch(BusinessException b){
            return  new ResponseEntity<>(b.getExceptionCode(),HttpStatus.BAD_REQUEST);
        }finally {
            time = (System.currentTimeMillis() - time);
            System.out.println("time deactivateUser:" +time+"ms");
        }
    }
}

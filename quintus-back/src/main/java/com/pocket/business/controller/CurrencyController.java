package com.pocket.business.controller;

import com.pocket.business.dto.CurrencyDTO;
import com.pocket.business.exception.BusinessException;
import com.pocket.business.service.CurrencyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/currency")
public class CurrencyController {

    @Autowired
    CurrencyService currencyService;
    @PostMapping(value = "insert")
    public ResponseEntity<?> insert(@RequestBody CurrencyDTO currencyDTO){
        try{
            CurrencyDTO currencyDTO1 = currencyService.insertCurrency(currencyDTO);
            return new ResponseEntity<>(currencyDTO1, HttpStatus.OK);
        } catch (BusinessException b){
            return new ResponseEntity<>(b.getExceptionCode(),HttpStatus.BAD_REQUEST);
        }
    }
}
